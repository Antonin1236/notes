package com.frerejacques.notesapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.Model.CategoryViewModel;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.Category;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapter;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapterCategory;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapterCategoryEdit;

import java.util.ArrayList;
import java.util.Calendar;

public class CategoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapterCategoryEdit recyclerViewAdapter;

    private CategoryViewModel categoryViewModel;



    private CardViewModel cardViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        categoryViewModel = new ViewModelProvider.AndroidViewModelFactory(
                getApplication()).create(CategoryViewModel.class);

        recyclerView = findViewById(R.id.cat_list_edit);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        recyclerView.setAdapter(recyclerViewAdapter);

        categoryViewModel.getAllCategories().observe(this, cards -> {
            recyclerViewAdapter = new RecyclerViewAdapterCategoryEdit(getApplicationContext(), cards);
            recyclerView.setAdapter(recyclerViewAdapter);
        });


        Button btn_import = findViewById(R.id.buttonAddCat);
        btn_import.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText CatName = findViewById(R.id.CatNameText);

                CategoryViewModel.insert(new Category(CatName.getText().toString(), false));


            }
        });




    }
}