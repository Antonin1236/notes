package com.frerejacques.notesapp.recyclerViewAdaptater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.R;
import com.frerejacques.notesapp.SecondFragment;
import com.frerejacques.notesapp.data.Card;
import com.google.android.material.chip.Chip;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final List<Card> cardList;
    private final FragmentActivity c;
    public RecyclerViewAdapter(FragmentActivity c, List<Card> cardList) {
        this.cardList = cardList;
        this.c = c;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.overview_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {
        Card card = cardList.get(position);

        holder.descriptionText.setText(card.description);
        holder.titleText.setText(card.title);
        holder.button.setText("Edit");


        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SecondFragment.selectedCard = card;
                Navigation.findNavController(c, R.id.nav_host_fragment_content_main).navigate(
                        R.id.action_FirstFragment_to_SecondFragment);
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardViewModel.delete(card);
            }
        });

        Locale locale = new Locale("fr", "FR");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String date = dateFormat.format(card.dateCreated);

        holder.dateChip.setText(date.toString());


    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView titleText;
        public TextView descriptionText;
        public Chip dateChip;
        public Button button;
        public Button deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.textView2);
            descriptionText = itemView.findViewById(R.id.textView3);
            dateChip = itemView.findViewById(R.id.date_clip);
            button = itemView.findViewById(R.id.edit_button);
            deleteButton = itemView.findViewById(R.id.delete);

        }
    }
}