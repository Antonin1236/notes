package com.frerejacques.notesapp.data;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class CategoryWithCards {
    @Embedded public Category category;
    @Relation(
            parentColumn = "category_id",
            entityColumn = "card_id",
            associateBy = @Junction(CardCategoryCrossRef.class)
    )
    public List<Card> cards;

}
