package com.frerejacques.notesapp.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CardDao {
    @Query("SELECT * FROM card_table")
    LiveData<List<Card>> getAll();

    //@Query("SELECT * FROM card_table where ")
    //LiveData<List<Card>> getAllActiveCard();

    @Query("SELECT * FROM card_table WHERE card_id = (:card_id) LIMIT 1")
    LiveData<Card> getById(int card_id);

    @Query("DELETE FROM card_table")
    void deleteAll();

    @Insert
    void insertAll(Card... cards);

    @Insert
    void insert(Card card);

    @Delete
    void delete(Card card);

    @Update
    void update(Card card);

    @Transaction
    @Query("SELECT * FROM card_table")
    public List<CardWithCategories> getCategories();


    @Transaction
    @Query("SELECT * FROM card_table WHERE card_id = (:card_id) LIMIT 1")
    CardWithCategories getCategoriesById(long card_id);

}
