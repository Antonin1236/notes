package com.frerejacques.notesapp.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CategoryDao {

    @Query("SELECT * FROM category_table")
    LiveData<List<Category>> getAll();

    @Query("SELECT * FROM category_table where is_active = 1")
    LiveData<List<Category>> getAllActive();

    @Query("SELECT * FROM category_table WHERE category_id = (:category_id) LIMIT 1")
    LiveData<Category> getById(int category_id);

    @Query("DELETE FROM card_table")
    void deleteAll();

    @Insert
    void insertAll(Category... categories);

    @Insert
    void insert(Category categories);

    @Delete
    void delete(Category categories);

    @Update
    void update(Category categories);

    @Transaction
    @Query("SELECT * FROM category_table")
    public List<CategoryWithCards> getCards();

    @Transaction
    @Query("SELECT * FROM category_table WHERE category_id = (:category_id) LIMIT 1")
    CategoryWithCards getCardsById(long category_id);
}

