package com.frerejacques.notesapp.Model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.frerejacques.notesapp.AppDatabase;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.CardCategoryCrossRef;
import com.frerejacques.notesapp.data.CardCategoryCrossRefDao;
import com.frerejacques.notesapp.data.CardDao;
import com.frerejacques.notesapp.data.Category;

import java.util.List;

public class CardCategoryCrossRefModel extends AndroidViewModel {
    private static CardCategoryCrossRefDao cardCategoryCrossRefDao;
    private static LiveData<List<Card>> allCard;

    public CardCategoryCrossRefModel(@NonNull Application application){
        super(application);
        AppDatabase database = AppDatabase.getDatabase(application);
        cardCategoryCrossRefDao = database.cardCategoryCrossRefDao();
    }

    public static void insert(CardCategoryCrossRef card){
        AppDatabase.databaseWriterExecutor.execute(()->  cardCategoryCrossRefDao.insert(card));
    }
    public static void delete(CardCategoryCrossRef card){
        AppDatabase.databaseWriterExecutor.execute(()-> cardCategoryCrossRefDao.delete(card));
    }
}