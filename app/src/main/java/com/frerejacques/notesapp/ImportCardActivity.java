package com.frerejacques.notesapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.databinding.ActivityMainBinding;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ImportCardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_card);
        Button btn_quit = findViewById(R.id.button_quit);
        btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button btn_import = findViewById(R.id.button_import);
        btn_import.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText code_text = findViewById(R.id.code_text);


                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url = "https://pastebin.com/raw/"+code_text.getText().toString();
                StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                String []res = response.split("@@@@@@@@@@");
                                if(res.length > 1){
                                    CardViewModel.insert(new Card(res[0], "red", res[1], Calendar.getInstance().getTime()));
                                    finish();
                                }

                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response","");
                            }
                        }
                );
                queue.add(postRequest);

            }
        });

    }


}