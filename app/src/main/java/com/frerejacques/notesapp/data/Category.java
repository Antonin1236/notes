package com.frerejacques.notesapp.data;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "category_table")
public class Category {
    @PrimaryKey(autoGenerate = true)
    public long category_id;

    public String name;

    public boolean is_active;


    public Category(String name, boolean is_active) {
        this.name = name;
        this.is_active = is_active;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    @Override
    public String toString() {
        return "Category{" +
                "category_id=" + category_id +
                ", name='" + name + '\'' +
                ", is_active=" + is_active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return category_id == category.category_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(category_id);
    }
}

