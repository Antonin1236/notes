package com.frerejacques.notesapp.Model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.frerejacques.notesapp.AppDatabase;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.CardDao;
import com.frerejacques.notesapp.data.CardWithCategories;
import com.frerejacques.notesapp.data.Category;

import java.util.List;

public class CardViewModel extends AndroidViewModel {
    private static CardDao cardDao;
    private static LiveData<List<Card>> allCard;

    public CardViewModel(@NonNull Application application){
        super(application);
        AppDatabase database = AppDatabase.getDatabase(application);
        cardDao = database.cardDao();

        allCard = cardDao.getAll();

    }

    public static void insert(Card card){
        AppDatabase.databaseWriterExecutor.execute(()->  cardDao.insert(card));

    }
    public  LiveData<List<Card>> getAllCard(){
        return  allCard;
    }

    public static void delete(Card card){
        AppDatabase.databaseWriterExecutor.execute(()-> cardDao.delete(card));
    }
    public static void update(Card card){
        AppDatabase.databaseWriterExecutor.execute(()-> cardDao.update(card));
    }
    public List<Category> getCategories(Card card){
        return cardDao.getCategoriesById(card.card_id).categories;
    }
}
