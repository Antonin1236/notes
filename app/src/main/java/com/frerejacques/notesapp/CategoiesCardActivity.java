package com.frerejacques.notesapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.frerejacques.notesapp.Model.CardCategoryCrossRefModel;
import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.Model.CategoryViewModel;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.Category;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapterCategoriesCard;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapterCategoryEdit;

public class CategoiesCardActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapterCategoriesCard recyclerViewAdapter;

    private CategoryViewModel categoryViewModel;
    private CardViewModel cardViewModel;
    private CardCategoryCrossRefModel cardCategoryCrossRefModel;

    public static Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoies_card);


        categoryViewModel = new ViewModelProvider.AndroidViewModelFactory(
                getApplication()).create(CategoryViewModel.class);


        cardCategoryCrossRefModel = new ViewModelProvider.AndroidViewModelFactory(
                getApplication()).create(CardCategoryCrossRefModel.class);

        recyclerView = findViewById(R.id.card_cat_list_edit);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        recyclerView.setAdapter(recyclerViewAdapter);



        cardViewModel = new ViewModelProvider.AndroidViewModelFactory(
                getApplication()).create(CardViewModel.class);

        categoryViewModel.getAllCategories().observe(this, cards -> {
            recyclerViewAdapter = new RecyclerViewAdapterCategoriesCard(getApplicationContext(), cards, cardViewModel.getCategories(card), card);
            recyclerView.setAdapter(recyclerViewAdapter);
        });


    }
}