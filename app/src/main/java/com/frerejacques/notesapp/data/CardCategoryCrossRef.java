package com.frerejacques.notesapp.data;

import androidx.room.Entity;

@Entity(primaryKeys = {"card_id", "category_id"})
public class CardCategoryCrossRef {
    public long category_id;
    public long card_id;

    public CardCategoryCrossRef(long category_id, long card_id) {
        this.category_id = category_id;
        this.card_id = card_id;
    }
}
