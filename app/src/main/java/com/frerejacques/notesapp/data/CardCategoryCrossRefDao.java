package com.frerejacques.notesapp.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface CardCategoryCrossRefDao {

    @Query("DELETE FROM CardCategoryCrossRef")
    void deleteAll();

    @Insert
    void insert(CardCategoryCrossRef cardCategoryCrossRef);

    @Delete
    void delete(CardCategoryCrossRef cardCategoryCrossRef);
}
