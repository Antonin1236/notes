package com.frerejacques.notesapp.data;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class CardWithCategories {
    @Embedded
    public Card card;
    @Relation(
            parentColumn = "card_id",
            entityColumn = "category_id",
            associateBy = @Junction(CardCategoryCrossRef.class)
    )
    public List<Category> categories;

}
